{ stdenvNoCC, fetchurl, pkgs, ... }:

let
  version = "0.11.0";
  platform =
    if pkgs.system == "x86_64-linux" then "x86_64-unknown-linux-musl"
    else if pkgs.system == "aarch64-darwin" then "aarch64-apple-darwin"
    else throw "unsupported system";
in
stdenvNoCC.mkDerivation {
  inherit version;
  name = "numi-cli";

  sourceRoot = ".";

  src = fetchurl {
    url = "https://github.com/nikolaeu/numi/releases/download/cli-v${version}/numi-cli-v${version}-${platform}.tar.gz";
    sha256 =
      if pkgs.system == "x86_64-linux" then "sha256-pYhDXqmWYeDqfs8mTl59y6HMxjt0UaLYV0tni5S3e3E="
      else if pkgs.system == "aarch64-darwin" then "sha256-4t+INHzATEuGy2jl/Ip3Tmgu90v0T7x4gjEH9yECUqk="
      else throw "unsupported system";
  };

  installPhase = "install -D numi-cli $out/bin/numi-cli";
}

