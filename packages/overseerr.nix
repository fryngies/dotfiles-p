{ fetchYarnDeps
, fetchFromGitHub
, stdenv
, nodejs
, python3
, nodePackages
, yarn2nix-moretea
, yarn
, jq
, lib
, makeWrapper
, ...
}:

let
  yarnNoCypress = yarn.overrideAttrs (_: { shellHook = "export CYPRESS_INSTALL_BINARY=0"; });
in
stdenv.mkDerivation rec {
  pname = "overseerr";
  version = "1.33.2";

  yarn = yarnNoCypress;

  offlineCache = fetchYarnDeps {
    yarnLock = "${src}/yarn.lock";
    sha256 = "sha256-SZwhC6djgU5qshtDhQnkz/INeklp/c+BKjn7ao0r5IE=";
  };

  deps =
    let
      pkgConfig = {
        sqlite3 = {
          postInstall = ''
            export CPPFLAGS="-I${nodejs}/include/node"
            node-pre-gyp install --fallback-to-build --nodedir=${nodejs}/include/node
          '';
        };

        bcrypt = {
          postInstall = ''
            export CPPFLAGS="-I${nodejs}/include/node"
            node-pre-gyp install --fallback-to-build --nodedir=${nodejs}/include/node
          '';
        };
      };

      postInstall = (builtins.map
        (key:
          if (pkgConfig.${key} ? postInstall) then
            ''
              for f in $(find -L -path '*/node_modules/${key}' -type d); do
                (cd "$f" && (${pkgConfig.${key}.postInstall}))
              done
            ''
          else
            ""
        )
        (builtins.attrNames pkgConfig));
    in
    stdenv.mkDerivation {
      pname = "overseerr-modules";
      inherit src version;

      nativeBuildInputs = [
        nodejs
        yarn
        yarn2nix-moretea.fixup_yarn_lock
        nodePackages.node-pre-gyp
        python3
      ];

      configurePhase = ''
        export HOME=$(mktemp -d)
        
        yarn config --offline set yarn-offline-mirror ${offlineCache}
        fixup_yarn_lock yarn.lock
      '';

      buildPhase = ''
        yarn install --offline \
          --frozen-lockfile \
          --ignore-engines \
          --ignore-scripts
        patchShebangs .

        ${lib.concatStringsSep "\n" postInstall}
      '';

      installPhase = ''
        mkdir -p $out
        cp -R node_modules package.json yarn.lock $out
      '';
    };

  src = fetchFromGitHub {
    owner = "sct";
    repo = "overseerr";
    rev = "v${version}";
    sha256 = "sha256-xDzWyU4f56+0Tpk87LpH6zXtxmRxVMCKySCY6WD5go0=";
  };

  nativeBuildInputs = [
    makeWrapper
    nodejs
    yarn
    jq
  ];

  buildInputs = [
    nodejs
  ];

  configurePhase = ''
    runHook preConfigure

    export HOME=$(mktemp -d) NODE_ENV=production

    yarn config --offline set yarn-offline-mirror ${offlineCache}

    mkdir -p $out

    cp -R ${deps}/{node_modules,yarn.lock} .
    chmod u+w -R node_modules

    runHook postConfigure
  '';

  buildPhase = ''
    runHook preBuild

    yarn --offline build
    # shrink by a few hundreds mbs
    # not doing full production reinstall to avoid rebuilding native modules
    jq '.devDependencies | keys[]' package.json | xargs printf -- 'node_modules/%s\n' | xargs rm -rf node_modules/{@next/swc*,@types,@commitlint,@typescript-eslint}

    runHook postBuild
  '';

  distPhase = "true";
  dontCheckForBrokenSymlinks = true;

  installPhase = ''
    runHook preInstall

    mv dist public node_modules .next next.config.js package.json overseerr-api.yml $out

    mkdir $out/bin
    cat > $out/bin/overseerr <<EOF
      #!${stdenv.shell}/bin/sh
      pushd $out
      ${nodejs}/bin/node $out/dist/index.js
      popd
    EOF
    chmod +x $out/bin/overseerr
    wrapProgram $out/bin/overseerr

    runHook postInstall
  '';

  meta = with lib; {
    description = "Self-hosted web application that automatically gives your shared Plex users the ability to request content by themselves";
    homepage = "https://overseerr.dev/";
    license = licenses.mit;
    platforms = [
      "x86_64-linux"
      "aarch64-linux"
      "aarch64-darwin"
    ];
  };
}
