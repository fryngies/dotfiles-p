{ pkgs }:

{
  telescope-git-diffs-nvim = pkgs.vimUtils.buildVimPlugin {
    pname = "telescope-git-diffs.nvim";
    version = "366df26227e6d478d5c55e04771d61875c4f22ac";
    src = pkgs.fetchFromGitHub {
      owner = "paopaol";
      repo = "telescope-git-diffs.nvim";
      rev = "366df26227e6d478d5c55e04771d61875c4f22ac";
      sha256 = "sha256-N9JuCoozIxtGiaFBORVApD6L4leggPpVKEP2TNhwRiw=";
    };
    meta.homepage = "https://github.com/paopaol/telescope-git-diffs.nvim";
  };
}
