{ config
, pkgs
, lib
, ...
}:
with lib; let
  cfg = config.services.pz-server;
in
{
  options.services.pz-server = {
    enable = mkEnableOption "enable the project zomboid server";

    serverDir = mkOption {
      type = types.path;
      description = "the path where server data will be stored at and the server binaries will be installed onto";
    };

    serverName = mkOption {
      type = types.nonEmptyStr;
      description = "the name of the ini file to use when starting up the server";
    };

    adminUserName = mkOption {
      type = types.nonEmptyStr;
      description = "the admin username";
    };

    adminPassword = mkOption {
      type = types.str;
      description = "the admin password";
    };

    user = mkOption {
      type = types.str;
      description = "the user the server should run as";
      default = "pzuser";
    };

    group = mkOption {
      type = types.str;
      description = "the group the server should run as";
      default = "pzuser";
    };

    tcpPort = mkOption {
      type = types.port;
      description = "the tcp port the server should run on";
      default = 16261;
    };

    udpPort = mkOption {
      type = types.port;
      description = "the udp port the server should run on";
      default = 16262;
    };

    steamPort1 = mkOption {
      type = types.port;
      description = "the first Steam port the server will use";
      default = 16263;
    };

    steamPort2 = mkOption {
      type = types.port;
      description = "the second Steam port the server will use";
      default = 16264;
    };

    steamVac = mkOption {
      type = types.bool;
      description = "enable/disable VAC on Steam servers";
      default = true;
    };

    updateOnStart = mkOption {
      type = types.bool;
      description = "whether to force server update on unit start";
      default = false;
    };

    openFirewall = mkOption {
      type = types.bool;
      description = "whether to open the required ports in the firewall for the server";
      default = false;
    };

    extraArgs = mkOption {
      type = types.attrsOf types.str;
      description = "additional parameters added to the launch script. Must be either JVM or Project Zomboid server arguments";
      default = { };
    };
  };

  config = mkIf cfg.enable {
    users.users = mkIf (cfg.user == "pzuser") {
      "${cfg.user}" = {
        group = cfg.group;
        home = cfg.serverDir;
        isSystemUser = true;
      };
    };

    users.groups = mkIf (cfg.group == "pzuser") {
      "${cfg.group}" = { gid = null; };
    };

    networking.firewall = mkIf cfg.openFirewall {
      allowedTCPPorts = [ cfg.tcpPort cfg.udpPort cfg.steamPort1 cfg.steamPort2 ];
      allowedUDPPorts = [ cfg.tcpPort cfg.udpPort cfg.steamPort1 cfg.steamPort2 ];
    };

    systemd.tmpfiles.rules = [
      "d '${cfg.serverDir}' 0755 ${cfg.user} ${cfg.group}"
    ];

    nixpkgs.config.allowUnfreePredicate = pkg:
      builtins.elem (lib.getName pkg) [
        "steamcmd"
        "steam-original"
      ];

    systemd.services.pz-server =
      let
        inherit (pkgs) stdenv libstdcxx5 zlib patchelf;
        inherit (pkgs.lib) makeLibraryPath;
        inherit (pkgs.steamPackages) steamcmd;
        steamcmdScript = pkgs.writeText "update_zomboid.txt" ''
          @ShutdownOnFailedCommand 1
          @NoPromptForPassword 1
          force_install_dir ${cfg.serverDir}
          login anonymous
          ${lib.optionalString cfg.updateOnStart "app_update 380870 validate"}
          quit
        '';
        libraryPath =
          "${stdenv.cc.cc.lib}/lib64:${cfg.serverDir}/linux64:${cfg.serverDir}/natives/lib:${cfg.serverDir}:${cfg.serverDir}/jre64/lib:"
          + makeLibraryPath [
            libstdcxx5 # libstdc++.so.6 libdl.so.2 libm.so.6 libgcc_s.so.1 libc.so.6 libpthread.so.0
            zlib # libz.so.1
          ];
        attrSetToLaunchArgs = lib.attrsets.mapAttrsToList (key: value: "-${key} \"${value}\"");
        knownArgs = {
          servername = cfg.serverName;
          adminusername = cfg.adminUserName;
          adminpassword = cfg.adminPassword;
          port = toString cfg.tcpPort;
          udpport = toString cfg.udpPort;
          steamvac = boolToString cfg.steamVac;
          steamport1 = toString cfg.steamPort1;
          steamport2 = toString cfg.steamPort2;
        };
      in
      {
        after = [ "network.target" ];
        description = "Project Zomboid Server";
        wantedBy = [ "multi-user.target" ];
        path = [
          steamcmd
          "${cfg.serverDir}/jre64"
        ];
        preStart = ''
          ${steamcmd}/bin/steamcmd +runscript ${steamcmdScript}
          ${patchelf}/bin/patchelf \
            --set-interpreter "$(cat ${stdenv.cc}/nix-support/dynamic-linker)" \
            --set-rpath "${libraryPath}" \
            "${cfg.serverDir}/ProjectZomboid64" \
            "${cfg.serverDir}/jre64/bin/java"
        '';
        script = ''
          export PATH="${cfg.serverDir}/jre64/bin:$PATH"
          JSIG="libjsig.so"
          LD_PRELOAD="''${LD_PRELOAD}:''${JSIG}" ${cfg.serverDir}/ProjectZomboid64 \
          ${builtins.concatStringsSep " \\\n" (
            (attrSetToLaunchArgs knownArgs) ++ (attrSetToLaunchArgs cfg.extraArgs)
          )}
        '';
        environment = {
          LD_LIBRARY_PATH = libraryPath;
        };
        serviceConfig = {
          User = cfg.user;
          Group = cfg.group;
          Restart = "always";
          WorkingDirectory = cfg.serverDir;
          TimeoutSec = "15min";
        };
      };
  };
}
