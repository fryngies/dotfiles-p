{ config, pkgs, lib, ... }:

with lib;

let
  cfg = config.services.overseerr;
  package = pkgs.overseerr;
in
{
  options = {
    services.overseerr = {
      enable = mkEnableOption (
        ''
          Overseerr.
          Optionally see <https://docs.overseerr.app/info/reverse-proxy>
          on how to set up a reverse proxy
        ''
      );

      dataDir = mkOption {
        type = types.str;
        default = "/var/lib/overseerr";
        description = "The directory where Overseerr stores its data files.";
      };

      port = mkOption {
        type = types.port;
        default = 5055;
        description = "The port for the Overseerr web interface.";
      };

      openFirewall = mkOption {
        type = types.bool;
        default = false;
        description = "Open ports in the firewall for the Overseerr web interface.";
      };

      user = mkOption {
        type = types.str;
        default = "overseerr";
        description = "User account under which Overseerr runs.";
      };

      group = mkOption {
        type = types.str;
        default = "overseerr";
        description = "Group under which Overseerr runs.";
      };
    };
  };

  config = mkIf cfg.enable {
    systemd.tmpfiles.rules = [
      "d '${cfg.dataDir}' 0700 ${cfg.user} ${cfg.group} - -"
    ];

    systemd.services.overseerr = {
      description = "Overseerr";
      after = [ "network.target" ];
      wantedBy = [ "multi-user.target" ];
      environment = {
        # see sources
        CONFIG_DIRECTORY = cfg.dataDir;
        PORT = toString cfg.port;
        # complains about missing `pages` directiory otherwise
        NODE_ENV = "production";
      };

      serviceConfig = {
        Type = "simple";
        User = cfg.user;
        Group = cfg.group;
        ExecStart = "${package}/bin/overseerr";
        Restart = "on-failure";
      };
    };

    networking.firewall = mkIf cfg.openFirewall {
      allowedTCPPorts = [ cfg.port ];
    };

    users.users = mkIf (cfg.user == "overseerr") {
      overseerr = {
        isSystemUser = true;
        group = cfg.group;
        home = cfg.dataDir;
      };
    };

    users.groups = mkIf (cfg.group == "overseerr") { overseerr = { }; };
  };
}
