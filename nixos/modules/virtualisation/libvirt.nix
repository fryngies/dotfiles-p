{ lib, config, ... }:

with lib;

let
  cfg = config.virtualisation.libvirtd;
in
{
  options.virtualisation.libvirtd = {
    deviceACL = mkOption {
      type = types.listOf types.str;
      default = [ ];
    };
    clearEmulationCapabilities = mkOption {
      type = types.bool;
      default = true;
    };
  };

  config.users.users."qemu-libvirtd" = {
    extraGroups = optionals (!cfg.qemu.runAsRoot) [ "kvm" "input" ];
    isSystemUser = true;
  };
}
