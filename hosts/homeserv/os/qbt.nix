{ pkgs, ... }:

{
  users.users = {
    qbittorrent = {
      isNormalUser = true;
      home = "/home/qbtuser";
    };
  };

  systemd = {
    tmpfiles.rules = [
      "d '/data/media/Downloads' 0755 qbittorrent users - -"
    ];

    packages = [ pkgs.qbittorrent-nox ];

    services."qbittorrent-nox@qbittorrent" = {
      enable = true;
      serviceConfig = {
        Type = "simple";
        User = "qbittorrent";
        ExecStart = "${pkgs.qbittorrent-nox}/bin/qbittorrent-nox --webui-port=8181";
      };
      wantedBy = [ "multi-user.target" ];
    };
  };

  networking.firewall.allowedTCPPorts = [
    # for incoming connections
    41280
  ];

  networking.firewall.allowedUDPPorts = [
    # for incoming connections
    41280
  ];
}
