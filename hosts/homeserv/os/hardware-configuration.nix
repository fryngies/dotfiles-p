{ config, lib, modulesPath, pkgs, ... }:

{
  imports =
    [
      (modulesPath + "/installer/scan/not-detected.nix")
    ];

  boot.initrd.availableKernelModules = [ "xhci_pci" "ahci" "nvme" "sd_mod" ];
  boot.initrd.kernelModules = [ "dm-snapshot" ];
  boot.kernelModules = [ "kvm-intel" ];
  boot.extraModulePackages = [ ];
  boot.kernel.sysctl = {
    "vm.swappiness" = 10;
  };

  services.btrfs.autoScrub.enable = true;

  fileSystems."/" =
    {
      device = "/dev/disk/by-uuid/c89545e8-ea8f-4205-8d36-859edf987f7c";
      fsType = "ext4";
      options = [ "noatime" "nodiratime" "discard" ];
    };

  fileSystems."/data/media" =
    {
      device = "/dev/disk/by-uuid/5a661a33-3eed-4412-86ce-88b0f5f3c5ac";
      fsType = "btrfs";
      options = [ "defaults" "nofail" "noatime" ];
    };

  fileSystems."/boot" =
    {
      device = "/dev/disk/by-uuid/F152-5498";
      fsType = "vfat";
    };

  fileSystems."/home" =
    {
      device = "/dev/disk/by-uuid/1b741278-c80a-416d-bc34-55e06a9b0ff7";
      fsType = "btrfs";
      options = [ "compress=zstd" "noatime" ];
    };

  swapDevices = [{
    device = "/var/lib/swapfile";
    size = 8 * 1024;
    randomEncryption.enable = true;
  }];

  systemd.services.hd-idle = {
    description = "External HD spin down daemon";
    wantedBy = [ "multi-user.target" ];
    serviceConfig = {
      ExecStart = "${pkgs.hd-idle}/bin/hd-idle -i 3600 -l /var/log/hd-idle.log";
      Restart = "always";
    };
  };

  nixpkgs.hostPlatform = lib.mkDefault "x86_64-linux";

  hardware = {
    cpu.intel.updateMicrocode = lib.mkDefault config.hardware.enableRedistributableFirmware;
    graphics = {
      enable = true;
      extraPackages = with pkgs; [
        intel-media-driver
        vaapiIntel
      ];
    };
  };
  # https://jellyfin.org/docs/general/administration/hardware-acceleration/intel/#known-issues-and-limitations
  # > Intel Gen 11 Jasper Lake and Elkhart Lake platforms (e.g. N5095, N5105, N6005, J6412) have quirks when using video encoders on Linux. The Low-Power Encoding mode MUST be configured and enabled for correct VBR and CBR bitrate control that is required by Jellyfin.
  # > Ticket: https://gitlab.freedesktop.org/drm/intel/-/issues/8080
  # without this fix, Plex can't use hardware acceleration
  boot.kernelParams = [ "i915.enable_guc=2" ];
}
