{ config, lib, ... }:

let
  buildService = { name, port, proto ? "http", basicAuth ? null, extraConfig ? "" }: {
    virtualHosts."${name}".extraConfig = /* caddy */ ''
      reverse_proxy ${proto}://localhost:${builtins.toString port} {
        ${extraConfig}
      }

      ${lib.strings.optionalString (basicAuth != null) ''
      basicauth {
        ${basicAuth.user} ${basicAuth.passwordHash}
      }
      ''}
    '';
  };
in
{
  networking.firewall.allowedTCPPorts = [ 80 443 ];

  services.caddy = lib.mkMerge [
    {
      enable = true;
    }
    (buildService { name = "radarr.{$BASE_HOST}"; port = 7878; })
    (buildService { name = "sonarr.{$BASE_HOST}"; port = 8989; })
    (buildService { name = "bazarr.{$BASE_HOST}"; port = 6767; })
    (buildService { name = "prowlarr.{$BASE_HOST}"; port = 9696; })
    (buildService { name = "qbt.{$BASE_HOST}"; port = 8181; })
    (buildService { name = "sabnzbd.{$BASE_HOST}"; port = 7683; })
    (buildService {
      name = "p.{$BASE_HOST}";
      port = config.services.prometheus.port;
      basicAuth = {
        user = "{$P_AUTH_USER}";
        passwordHash = "{$P_PASSWORD_HASH}";
      };
    })
    (buildService { name = "g.{$BASE_HOST}"; port = config.services.grafana.settings.server.http_port; })
    (buildService { name = "{$TV_HOST}"; port = config.services.overseerr.port; })
    (buildService { name = "api.chikinicons.tomengine.ru"; port = config.services.chikin-icons-api.port; })
    {
      virtualHosts."pub.{$BASE_HOST}".extraConfig = ''
        file_server {
          root /data/share
        }
      '';
    }
  ];

  sops.secrets."caddy/env" = { };

  systemd.services.caddy.serviceConfig.EnvironmentFile = [ config.sops.secrets."caddy/env".path ];

  systemd.tmpfiles.rules = [
    "d /data/media/share 0755 ${config.services.caddy.user} ${config.services.caddy.group} - -"
  ];
}
