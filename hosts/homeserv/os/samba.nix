{
  services.avahi = {
    enable = true;
    openFirewall = true;
    nssmdns4 = true;
    publish.addresses = true;
    publish.domain = true;
    publish.enable = true;
    publish.userServices = true;
    publish.workstation = true;
    publish.hinfo = true;
    extraServiceFiles = {
      smb = ''
        <?xml version="1.0" standalone='no'?><!--*-nxml-*-->
        <!DOCTYPE service-group SYSTEM "avahi-service.dtd">
        <service-group>
          <name replace-wildcards="yes">%h</name>
          <service>
            <type>_smb._tcp</type>
            <port>445</port>
          </service>
        </service-group>
      '';
    };
  };

  services.samba = {
    enable = true;
    openFirewall = true;
    settings = {
      global = {
        workgroup = "WORKGROUP";
        "server string" = "homeserv";
        "netbios name" = "homeserv";
        "security" = "user";
        "hosts allow" = "192.168.178.0/24	localhost";
        "hosts deny" = "0.0.0.0/0";
        "guest account" = "nobody";
        "map to guest" = "bad user";
        browseable = "yes";
      };
      homes = {
        browseable = "no";
        "read only" = "no";
        "guest ok" = "no";
      };
      media = {
        path = "/data/media/";
        "read only" = "yes";
        "guest ok" = "yes";
        browseable = "yes";
      };
    };
  };
}
