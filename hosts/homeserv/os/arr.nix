{ config, ... }:

let
  content-manager = "content-manager";
in
{
  users.groups.${content-manager} = { };

  services.sonarr = {
    enable = true;
    dataDir = "/var/lib/sonarr";
    user = "sonarr";
    group = "sonarr";
  };
  users.users.sonarr.extraGroups = [ content-manager ];

  services.radarr = {
    enable = true;
    dataDir = "/var/lib/radarr";
    user = "radarr";
    group = "radarr";
  };
  users.users.radarr.extraGroups = [ content-manager ];

  services.prowlarr.enable = true;

  services.bazarr.enable = true;

  services.overseerr = {
    enable = true;
    openFirewall = true;
  };

  services.sabnzbd = {
    enable = true;
    configFile = "/var/lib/sabnzbd/sabnzbd.ini";
    user = "sabnzbd";
    group = content-manager;
  };

  services.plex = {
    enable = true;
    openFirewall = true;
    dataDir = "/var/lib/plexmediaserver";
  };

  networking.firewall.allowedTCPPorts = [
    # sabnzbd
    7683
  ];

  systemd.tmpfiles.rules = [
    "d '/data/media/TV Shows' 755 ${config.services.sonarr.user} ${config.services.sonarr.group}"
    "d '/data/media/Movies' 755 ${config.services.radarr.user} ${config.services.radarr.group}"
    "d '/data/media/usenet' 755 ${config.services.sabnzbd.user} ${config.services.sabnzbd.group}"
    "d '/home/.sabnzbd-incomplete' 755 ${config.services.sabnzbd.user} ${config.services.sabnzbd.group}"

    # Allow Bazarr to place subtitles and Plex to store optimized versions
    "a '/data/media/TV Shows' - - - - d:user:${config.services.bazarr.user}:rwx,d:user:${config.services.plex.user}:rwx,d:m::rwx"
    "a '/data/media/Movies' - - - - d:user:${config.services.bazarr.user}:rwx,d:user:${config.services.plex.user}:rwx,d:m::rwx"

    # Allow Sonarr and Radarr to remove downloads
    "d '/data/media/usenet/Downloads/complete/' 775 ${config.services.sabnzbd.user} ${config.services.sabnzbd.group}"

    "d '/var/lib/sabnzbd' 700 ${config.services.sabnzbd.user} ${config.services.sabnzbd.group}"
  ];
}
