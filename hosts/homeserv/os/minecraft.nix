{ pkgs, ... }:

{
  services.minecraft-server = {
    enable = false;
    eula = true;
    declarative = true;
    openFirewall = true;
    package = pkgs.minecraft-server.override {
      # curl https://launchermeta.mojang.com/mc/game/version_manifest.json | jq | head -50
      # 1.20.5
      version = "1.20.5";
      url = "https://piston-data.mojang.com/v1/objects/79493072f65e17243fd36a699c9a96b4381feb91/server.jar";
      sha1 = "79493072f65e17243fd36a699c9a96b4381feb91";
      jre_headless = pkgs.openjdk22_headless;
    };
    serverProperties = {
      difficulty = 3;
      view-distance = 20;
    };
  };

  #   users.users.ftb = {
  #     description = "Minecraft server service user";
  #     home = "/var/lib/ftb-direwolf";
  #     createHome = true;
  #     isSystemUser = true;
  #     group = "ftb";
  #   };
  #   users.groups.ftb = { };
  # 
  #   systemd.services.ftb-server = {
  #     description = "FTB Server Service";
  #     wantedBy = [ "multi-user.target" ];
  #     after = [ "network.target" ];
  #     path = [ pkgs.openjdk17_headless pkgs.lazymc ];
  # 
  #     serviceConfig = {
  #       ExecStart = "${pkgs.lazymc}/bin/lazymc start";
  #       ExecStop = "$MAINPID";
  #       Restart = "always";
  #       User = "ftb";
  #       WorkingDirectory = "/var/lib/ftb-direwolf";
  # 
  #       # StandardInput = "socket";
  #       StandardOutput = "journal";
  #       StandardError = "journal";
  # 
  #       # Hardening
  #       CapabilityBoundingSet = [ "" ];
  #       DeviceAllow = [ "" ];
  #       LockPersonality = true;
  #       PrivateDevices = true;
  #       PrivateTmp = true;
  #       PrivateUsers = true;
  #       ProtectClock = true;
  #       ProtectControlGroups = true;
  #       ProtectHome = true;
  #       ProtectHostname = true;
  #       ProtectKernelLogs = true;
  #       ProtectKernelModules = true;
  #       ProtectKernelTunables = true;
  #       ProtectProc = "invisible";
  #       RestrictAddressFamilies = [ "AF_INET" "AF_INET6" ];
  #       RestrictNamespaces = true;
  #       RestrictRealtime = true;
  #       RestrictSUIDSGID = true;
  #       SystemCallArchitectures = "native";
  #       UMask = "0077";
  #     };
  #   };
  # 
  #   networking.firewall = {
  #     allowedUDPPorts = [ 25565 ];
  #     allowedTCPPorts = [ 25565 ];
  #   };
}
