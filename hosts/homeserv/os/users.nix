{ config, pkgs, ... }:

{
  security.polkit.enable = true;

  # Set user secrets to be accessable before NixOS creates users
  sops.secrets."passwd/users/fryngies/pwd".neededForUsers = true;

  users = {
    defaultUserShell = pkgs.zsh;
    mutableUsers = false;

    users = {
      root = {
        # Disable root login
        hashedPassword = "*";
      };

      fryngies = {
        isNormalUser = true;
        extraGroups = [ "wheel" "libvirtd" ];
        hashedPasswordFile = config.sops.secrets."passwd/users/fryngies/pwd".path;

        openssh.authorizedKeys.keys = [
          "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC3Cln5r3EdLndWzsyyRllJnb73Hj8OJds0VDwPd5DZFIop6GvWHgP6taSxGchTfU9W1kDRwlFt056hFD6LEZIag/QYpTiedIclBippDWkomLsCAXaEAqBjpOlzbWmJ87SBBPZ/i85BohJpOucOy37rZUM6NbtdTbd+bvj7BTM3VHjtpIbeju7fr+FfOWgi0+MNOkRVCgWtLVrt8Z0JAV5EFrZPFneqa+UpNPm86NHV31F7qF0DdMPpsnTTLX18fY+OQqLp8mP8FjT4R/skur4m8G+ACBIUimQkZp5lEOJopUWkxWRMrx5oNniuBire/StJ0Hs+a9QwkRfdr0FzSXzOIWxH6KY6S5C/chdK8Y6pcvFLYlAqU4NYj9GqhbShXTXPgqYdIwfmzlJBbWG85l3aXU5ZVVAOScxpzAkjPSOlpz1h7Kmw0A4UZQa82XVSKGZP01al1AZGFl2Stb2phXj+rPaaq2apXeS+DbnpUgV+ydliH+jAUszIG0JYUFm0MH5JhKox+/MxDzlpAcnPTOof5pve1PecLwcBLm5FWwokZ7PBJCK60Fcpabhr+o3zui8fJVRRVA42ZqSAw0OtDjkRnxe3J7o/omzNHvrreiekAk3i93ag8l6gxJxrR2aHUkfHvbDg3+lnSzmMU7wZYb2gjiRxlxiblpvf1SNfGpyPzw== fryngies@Mac-Pro-Dmitrij.local-"
          "ecdsa-sha2-nistp256 AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBOnKKJauwm/iELQ9CSq8dz/9FmL+x+1Fb4livEVJvDMLETzDTYqRSaqw4CzZFl3+2GqwyF4lDcekcHaWJ4KVWoQ= easy@secretive.NB-Y294QDQ9HG.local"
        ];
      };
    };
  };

  security.pam.sshAgentAuth.enable = true;
  # remove when default is updated https://github.com/NixOS/nixpkgs/pull/277626
  security.pam.sshAgentAuth.authorizedKeysFiles = [ "/etc/ssh/authorized_keys.d/%u" ];
}
