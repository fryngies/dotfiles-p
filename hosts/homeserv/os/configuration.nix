{ pkgs, config, ... }:

{
  imports =
    [
      ./hardware-configuration.nix
      ./qbt.nix
      ./arr.nix
      ./caddy.nix
      ./chikin-icons.nix
      ./samba.nix
      ./users.nix
      ./minecraft.nix
      ./dyndns.nix
      ./monitoring.nix
    ];

  sops.defaultSopsFile = ./secrets.yaml;

  boot = {
    loader.systemd-boot.enable = true;
    loader.efi.canTouchEfiVariables = true;

    kernel = {
      sysctl = {
        "net.ipv4.conf.all.forwarding" = true;
        "net.ipv6.conf.all.forwarding" = true;
      };
    };
  };

  hardware.cpu.intel.updateMicrocode = true;

  networking.useDHCP = false;

  system.autoUpgrade = {
    enable = true;
    dates = "04:00";
    flake = "${config.users.users.fryngies.home}/dotfiles/nix";
    flags = [
      "--update-input"
      "nixpkgs"
    ];
    allowReboot = true;
  };

  systemd.services.nixos-upgrade.environment.GIT_SSH_COMMAND = "ssh -i ${config.users.users.fryngies.home}/.ssh/id_rsa";

  systemd.network = {
    enable = true;

    networks."10-enp3s0" = {
      enable = true;

      matchConfig = {
        Name = "enp3s0";
      };

      networkConfig = {
        DHCP = "yes";
        DNS = [ "1.1.1.1" ];
        DNSOverTLS = "yes";
        DNSSEC = "allow-downgrade";
      };
    };
  };

  networking.hostName = "homeserv";

  time.timeZone = "Europe/Amsterdam";

  programs.zsh.enable = true;

  programs.neovim = {
    enable = true;
    defaultEditor = true;
  };

  environment.systemPackages = with pkgs; [
    bash
    git
    libva
    parted
  ];

  programs.gnupg.agent = {
    enable = true;
  };

  services.openssh.enable = true;

  virtualisation = {
    libvirtd = {
      enable = true;

      qemu = {
        runAsRoot = true;
        swtpm.enable = true;

        ovmf = {
          enable = true;
          packages = [
            (pkgs.OVMF.override {
              secureBoot = true;
              tpmSupport = true;
            }).fd
          ];
        };
      };

      onBoot = "ignore";
      onShutdown = "shutdown";
    };
  };

  services.pz-server = {
    enable = false;
    serverDir = "/var/lib/zomboid";
    adminUserName = "admin";
    adminPassword = "1234";
    serverName = "ne ori";
    tcpPort = 8766;
    udpPort = 8767;
    steamPort1 = 8768;
    steamPort2 = 8769;
    openFirewall = true;
    updateOnStart = true;
  };

  services.borgbackup.jobs."borgbase" = {
    extraArgs = "--debug";
    paths = [
      "/var/lib/overseerr"
      "/var/lib/plexmediaserver"
      "/var/lib/radarr"
      "/var/lib/sonarr"
      "/var/lib/bazarr"
      "/var/lib/private/prowlarr"
    ];

    exclude =
      let
        listIn = base: paths: (map (p: "${base}/${p}") paths);
      in
      listIn "/var/lib/overseerr" [ "cache" "logs" ] ++
      listIn "/var/lib/plexmediaserver/Library/Application Support/Plex Media Server" [ "Media" "Cache" "Metadata" "Logs" ] ++
      listIn "/var/lib/radarr" [ "MediaCover" "logs" ] ++
      listIn "/var/lib/sonarr" [ "MediaCover" "logs" ] ++
      listIn "/var/lib/bazarr" [ "backup" "log" "cache" ".cache" ] ++
      listIn "/var/lib/private/prowlarr" [ "Backups" "logs" ] ++
      [ ];

    repo = "ssh://c32m2fft@c32m2fft.repo.borgbase.com/./repo";
    encryption = {
      mode = "repokey-blake2";
      passCommand = "cat /root/borgbackup/passphrase";
    };
    environment.BORG_RSH = "ssh -i /root/borgbackup/ssh_key";
    prune.keep.within = "1m";
    compression = "auto,lzma";
    startAt = "daily";
  };

  systemd.services.libvirtd.preStart =
    let
      qemuHook = pkgs.writeScript "qemu-hook" ''
        #!${pkgs.stdenv.shell}

        command=$2

        if [ "$command" = "started" ]; then
            systemctl set-property --runtime -- system.slice AllowedCPUs=3
            systemctl set-property --runtime -- user.slice AllowedCPUs=3
            systemctl set-property --runtime -- init.scope AllowedCPUs=3
        elif [ "$command" = "release" ]; then
            systemctl set-property --runtime -- system.slice AllowedCPUs=0-3
            systemctl set-property --runtime -- user.slice AllowedCPUs=0-3
            systemctl set-property --runtime -- init.scope AllowedCPUs=0-3
        fi
      '';
    in
    ''
      mkdir -p /var/lib/libvirt/hooks
      chmod 755 /var/lib/libvirt/hooks

      # Copy hook files
      ln -sf ${qemuHook} /var/lib/libvirt/hooks/qemu
    '';

  system.stateVersion = "22.11";
}
