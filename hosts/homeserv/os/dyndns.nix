{ pkgs, config, ... }:

{
  sops.secrets = {
    "caddy/env" = { };
    "dns/key" = { };
    "dns/record" = { };
    "dns/domain" = { };
  };

  systemd.services.dyndns = {
    description = "Run DNS records update";
    wantedBy = [ "multi-user.target" ];
    after = [ "network.target" ];
    path = [ pkgs.curl pkgs.jq ];

    script = ''
      gdapikey=`cat ${config.sops.secrets."dns/key".path}`
      record=`cat ${config.sops.secrets."dns/record".path}`
      domain=`cat ${config.sops.secrets."dns/domain".path}`

      myip=`curl -s "https://api.ipify.org"`
      dnsdata=`curl -s -X GET -H "Authorization: Bearer $gdapikey" -H "Content-Type:application/json" "https://api.cloudflare.com/client/v4/zones/$domain/dns_records/$record"`
      gdip=`echo $dnsdata | jq -r '.result.content'`
      echo "`date '+%Y-%m-%d %H:%M:%S'` - Current External IP is $myip, DNS IP is $gdip"

      if [ "$gdip" != "$myip" -a "$myip" != "" -a "$gdip" != "null" ]; then
        echo "IP has changed!! Updating on DNS"
        curl -v -s -X PATCH \
          "https://api.cloudflare.com/client/v4/zones/$domain/dns_records/$record" \
          -H "Authorization: Bearer $gdapikey" \
          -H "Content-Type: application/json" \
          -d "{\"content\": \"$myip\"}"
      fi
    '';

    serviceConfig = {
      Type = "oneshot";
      EnvironmentFile = [ config.sops.secrets."caddy/env".path ];
    };
  };

  systemd.timers.dyndns = {
    description = "Run dns update";
    wantedBy = [ "timers.target" ];
    timerConfig = {
      OnCalendar = "daily";
      Persistent = "true";
    };
  };
}
