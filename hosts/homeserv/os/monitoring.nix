{ config, pkgs, ... }:

let
  mosquittoListener = {
    address = "0.0.0.0";
    port = 1883;
    users.root = {
      acl = [ "readwrite #" ];
      passwordFile = config.sops.secrets."mosquitto/password".path;
    };
  };
in
{
  services.grafana = {
    enable = true;

    settings = {
      server.http_port = 2342;
    };

    provision.datasources.settings = {
      apiVersion = 1;

      deleteDatasources = [
        {
          name = "prometheus";
          orgId = 1;
        }
        {
          name = "mosquitto";
          orgId = 1;
        }
      ];

      datasources =
        let
          mkSecretPath = p: "$__file{${config.sops.secrets.${p}.path}}";
        in
        [
          {
            name = "Prometheus";
            type = "prometheus";
            uid = "ddj0pu4tvn2m8f";
            orgId = 1;
            isDefault = true;
            editable = false;
            url = "http://${config.services.prometheus.listenAddress}:${toString config.services.prometheus.port}";
          }

          {
            name = "Mosquitto";
            type = "grafana-mqtt-datasource";
            uid = "fdjm04tlelxq8d";
            orgId = 1;
            editable = false;
            jsonData = {
              uri = "tcp://${mosquittoListener.address}:${toString mosquittoListener.port}";
              username = mkSecretPath "mosquitto/user";
            };
            secureJsonData.password = mkSecretPath "mosquitto/password";
          }
        ];
    };
  };

  services.prometheus = {
    enable = true;
    port = 2343;

    exporters = {
      systemd.enable = true;

      sabnzbd = {
        enable = true;
        listenAddress = "127.0.0.1";
        servers = [{
          baseUrl = "http://127.0.0.1:7683/sabnzbd";
          apiKeyFile = config.sops.secrets."apiKey/sabnzbd".path;
        }];
      };

      exportarr-sonarr = {
        enable = true;
        port = 9708;
        url = "http://127.0.0.1:8989/";
        apiKeyFile = config.sops.secrets."apiKey/sonarr".path;
      };

      exportarr-radarr = {
        enable = true;
        port = 9709;
        url = "http://127.0.0.1:7878/";
        apiKeyFile = config.sops.secrets."apiKey/radarr".path;
      };

      exportarr-prowlarr = {
        enable = true;
        port = 9710;
        url = "http://127.0.0.1:9696/";
        apiKeyFile = config.sops.secrets."apiKey/prowlarr".path;
      };

      exportarr-bazarr = {
        enable = true;
        port = 9711;
        url = "http://127.0.0.1:6767/";
        apiKeyFile = config.sops.secrets."apiKey/bazarr".path;
      };

      node = {
        enable = true;
        enabledCollectors = [ "systemd" ];
        port = 9002;
      };
    };

    scrapeConfigs = [
      {
        job_name = "chrysalis";
        static_configs = [{
          targets = [ "127.0.0.1:${toString config.services.prometheus.exporters.node.port}" ];
        }];
      }

      {
        job_name = "sabnzbd";
        honor_timestamps = true;
        scrape_interval = "5s";
        scrape_timeout = "5s";
        static_configs = [{
          targets = [ "${config.services.prometheus.exporters.sabnzbd.listenAddress}:${toString config.services.prometheus.exporters.sabnzbd.port}" ];
        }];
      }

      {
        job_name = "exportarr-sonarr";
        static_configs = [{
          targets = [ "${config.services.prometheus.exporters.exportarr-sonarr.listenAddress}:${toString config.services.prometheus.exporters.exportarr-sonarr.port}" ];
        }];
      }

      {
        job_name = "exportarr-radarr";
        static_configs = [{
          targets = [ "${config.services.prometheus.exporters.exportarr-radarr.listenAddress}:${toString config.services.prometheus.exporters.exportarr-radarr.port}" ];
        }];
      }

      {
        job_name = "exportarr-prowlarr";
        static_configs = [{
          targets = [ "${config.services.prometheus.exporters.exportarr-prowlarr.listenAddress}:${toString config.services.prometheus.exporters.exportarr-prowlarr.port}" ];
        }];
      }

      {
        job_name = "exportarr-bazarr";
        static_configs = [{
          targets = [ "${config.services.prometheus.exporters.exportarr-bazarr.listenAddress}:${toString config.services.prometheus.exporters.exportarr-bazarr.port}" ];
        }];
      }

      {
        job_name = "tasmota-exporter";
        metrics_path = "/probe";
        static_configs = [{
          targets = [
            "tasmota-boiler"
            "tasmota-homeserv"
          ];
        }];
        relabel_configs = [
          { source_labels = [ "__address__" ]; target_label = "__param_target"; }
          { source_labels = [ "__param_target" ]; target_label = "instance"; }
          { target_label = "__address__"; replacement = config.services.tasmota-exporter.listenAddr; }
        ];
      }
    ];
  };

  networking.extraHosts = ''
    192.168.178.137 tasmota-boiler
    192.168.178.207 tasmota-homeserv
  '';

  services.tasmota-exporter = {
    enable = true;
    listenAddr = "127.0.0.1:9712";
  };

  services.mosquitto = {
    enable = true;
    listeners = [ mosquittoListener ];
  };
  networking.firewall.allowedTCPPorts = [ 1883 ];

  systemd.services.grafana.serviceConfig.EnvironmentFile = config.sops.secrets."grafana/env".path;

  sops.secrets = {
    "grafana/env" = {
      reloadUnits = [ "grafana.service" ];
    };

    "mosquitto/user" = {
      owner = "grafana";
      group = "grafana";
    };

    "mosquitto/password" = {
      owner = "grafana";
      group = "grafana";
    };

    "apiKey/sabnzbd" = { };
    "apiKey/sonarr" = { };
    "apiKey/radarr" = { };
    "apiKey/prowlarr" = { };
    "apiKey/bazarr" = { };
  };
}
