{ pkgs, ... }:

{
  # Auto upgrade nix package and the daemon service.
  services.nix-daemon.enable = true;

  # Requires Homebrew to be installed
  system.activationScripts.preUserActivation.text = ''
    if ! xcode-select --version 2>/dev/null; then
      $DRY_RUN_CMD xcode-select --install
    fi
    if ! /opt/homebrew/bin/brew --version 2>/dev/null; then
      $DRY_RUN_CMD /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
    fi
  '';

  networking.hostName = "dg-mba";

  security.pam.enableSudoTouchIdAuth = true;

  users = {
    users = {
      fryngies = {
        shell = pkgs.zsh;
        description = "Dmitrii Gutman";
        home = "/Users/fryngies";
      };
    };
  };

  programs.zsh.enable = true;

  nix.linux-builder = {
    enable = false;
    ephemeral = true;
  };
  nix.settings.trusted-users = [ "@admin" ];

  homebrew = {
    enable = true;
    onActivation = {
      cleanup = "zap"; # Uninstall all programs not declared
      upgrade = true;
    };
    casks = [
      "bias-fx"
      "crossover"
      "eloston-chromium"
      "discord"
      "firefox"
      "figma"
      "flycut"
      "handbrake"
      "lunar"
      "iina"
      "iterm2"
      "megasync"
      "mumble"
      "notion"
      "orion"
      "ollama"
      "plex"
      "rustdesk"
      "zoom"
      "secretive"
      "steam"
      "telegram"
      "tor-browser"
      "utm"
      "visual-studio-code"
      "xld"
    ];
  };

  system.stateVersion = 5;
}
