{ pkgs, ... }:

{
  environment.systemPackages = with pkgs; [
    discord
    haruna
    handbrake
    lutris
    mumble
    kdePackages.partitionmanager
    telegram-desktop
    wineWowPackages.waylandFull
    winetricks
    wl-clipboard
  ];

  services = {
    displayManager = {
      sddm.enable = true;
      sddm.wayland.enable = true;
      autoLogin.user = "fryngies";
    };
    desktopManager.plasma6.enable = true;
  };

  programs.steam = {
    enable = true;
    remotePlay.openFirewall = true;
    dedicatedServer.openFirewall = true;
    extraCompatPackages = [ pkgs.proton-ge-bin ];
  };

  programs.kdeconnect.enable = true;
}

