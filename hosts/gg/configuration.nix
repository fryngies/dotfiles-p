{ pkgs, lib, config, ... }:

{
  imports = [
    ./hardware-configuration.nix
    ./disks.nix
    ./users.nix
    ./gui.nix
  ];

  boot = {
    kernelPackages = pkgs.linuxPackages_zen;
    kernelModules = [ "zenpower" ];
    extraModulePackages = [ pkgs.linuxPackages_zen.zenpower ];
    kernel.sysctl = {
      "vm.swappiness" = 10;
      # https://wiki.archlinux.org/title/Gaming#Tweaking_kernel_parameters_for_response_time_consistency
      "vm.compaction_proactiveness" = 0;
      "vm.watermark_boost_factor" = 1;
      "vm.min_free_kbytes" = 1048576;
      "vm.watermark_scale_factor" = 500;
    };

    loader = {
      efi.canTouchEfiVariables = true;
      timeout = 1;

      systemd-boot = {
        # Lanzaboote currently replaces the systemd-boot module.
        # This setting is usually set to true in configuration.nix
        # generated at installation time. So we force it to false
        # for now.
        enable = lib.mkForce false;
        consoleMode = "max";
        editor = false;
      };
    };
  };

  # on kernel 6.11 sleep is broken when bluetooth is on
  # https://bugzilla.redhat.com/show_bug.cgi?id=2314036
  # https://bugzilla.redhat.com/show_bug.cgi?id=2321268
  powerManagement.powerDownCommands = /* bash */''
    if [ -n "$(${pkgs.util-linux}/bin/rfkill list bluetooth|grep 'Soft blocked: no')" ]; then
      touch /tmp/bluetooth-was-enabled-before-suspend
      ${pkgs.util-linux}/bin/rfkill block bluetooth
    fi
  '';
  powerManagement.resumeCommands = /* bash */ ''
    if [ -f "/tmp/bluetooth-was-enabled-before-suspend" ]; then
      rfkill unblock bluetooth
      rm -f /tmp/bluetooth-was-enabled-before-suspend
    fi
  '';

  boot.lanzaboote = {
    enable = true;
    pkiBundle = "/etc/secureboot";
  };

  environment.etc."tmpfiles.d/gaming.conf".text = ''
    w /sys/kernel/mm/transparent_hugepage/enabled - - - - madvise
    w /sys/kernel/mm/transparent_hugepage/shmem_enabled - - - - advise
    w /sys/kernel/mm/transparent_hugepage/defrag - - - - never
    w /sys/kernel/mm/lru_gen/enabled - - - - 5
    w /proc/sys/vm/page_lock_unfairness - - - - 1
    w /proc/sys/kernel/sched_child_runs_first - - - - 0
    w /proc/sys/kernel/sched_autogroup_enabled - - - - 1
    w /proc/sys/kernel/sched_cfs_bandwidth_slice_us - - - - 3000
    w /sys/kernel/debug/sched/base_slice_ns  - - - - 3000000
    w /sys/kernel/debug/sched/migration_cost_ns - - - - 500000
    w /sys/kernel/debug/sched/nr_migrate - - - - 8
  '';

  services.avahi = {
    enable = true;
    nssmdns4 = true;

    publish = {
      enable = true;
      domain = true;
      hinfo = true;
    };
  };

  sops = {
    defaultSopsFile = ./secrets.yaml;
    age.sshKeyPaths = [ "/etc/ssh/ssh_host_ed25519_key" ];
  };

  hardware.enableAllFirmware = true;

  system.autoUpgrade = {
    enable = true;
    dates = "04:00";
    flake = "${config.users.users.fryngies.home}/dotfiles-p/nix";
    flags = [
      "--update-input"
      "nixpkgs"
    ];
    allowReboot = true;
  };

  networking = {
    useDHCP = false;
    networkmanager.enable = true;
    dhcpcd.enable = false;

    nameservers = [
      "1.1.1.1"
      "1.0.0.1"
      "8.8.8.8"
      "8.8.4.4"
    ];

    hostName = "gg";
  };

  time.timeZone = "Europe/Amsterdam";

  programs.zsh.enable = true;

  programs.neovim = {
    enable = true;
    defaultEditor = true;
  };

  programs.gnupg.agent = {
    enable = true;
  };

  services.openssh.enable = true;
  services.fwupd.enable = true;

  hardware.graphics = {
    enable = true;
    enable32Bit = true;
  };

  hardware.bluetooth.enable = true;

  documentation.dev.enable = true;

  environment.systemPackages = with pkgs; [
    nodejs

    man-pages
    man-pages-posix
  ];

  system.stateVersion = "24.05";
}

