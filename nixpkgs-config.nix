{ self, nixpkgs, ... }:

{
  nixpkgs.config.allowUnfree = true;

  nix = {
    settings.trusted-users = [ "fryngies" ];
    extraOptions = ''
      experimental-features = nix-command flakes
      builders-use-substitutes = true
      extra-trusted-public-keys = devenv.cachix.org-1:w1cLUi8dv3hnoSPGAuibQv+f9TZLr6cv/Hm9XgU50cw=
      extra-substituters = https://devenv.cachix.org
    '';
    registry = {
      self.flake = self;

      nixpkgs = {
        from = { id = "nixpkgs"; type = "indirect"; };
        flake = nixpkgs;
      };
    };
  };
}
