# [Nix](https://nixos.org) configuration

## Overview

This configuration uses Nix flakes.

- `home` — [home-manager](https://github.com/nix-community/home-manager) configurations split up by host. Includes modules for Neovim, iTerm and others
- `hosts` — system-wide configurations split up by host
- `keys` — sops public keys
- `overlays` — Nix overlays
