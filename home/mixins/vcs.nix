{
  programs.git = {
    enable = true;
    signing = {
      signByDefault = true;
      key = "4839AC419BD58392";
    };

    ignores = [ "*~" "*.swp" "tags" ".DS_Store" ".env.local" ".npm-local" ".direnv" ];

    difftastic.enable = true;
  };
}
