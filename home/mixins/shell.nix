{ lib, pkgs, ... }:

{
  home.shellAliases = lib.mkIf pkgs.hostPlatform.isDarwin {
    flushdns = "sudo dscacheutil -flushcache; sudo killall -HUP mDNSResponder";
  };

  programs.zsh = {
    enable = true;
    enableCompletion = true;
    autosuggestion.enable = true;
    history.expireDuplicatesFirst = true; # Expire duplicate entries first when trimming history.

    initExtra = ''
      ps1_level=""
      if [[ $SHLVL > 1 ]]; then
        ps1_level="<$SHLVL>"
      fi

      PS1="$ps1_level $PS1"
    '';

    prezto = {
      enable = true;
      pmodules = [
        "environment"
        "terminal"
        "editor"
        "history"
        "directory"
        "spectrum"
        "utility"
        "completion"
        "prompt"
        "ssh"
        "git"
        "syntax-highlighting"
        "docker"
        "history-substring-search"
      ] ++ lib.optionals pkgs.hostPlatform.isDarwin [ "homebrew" ];

      editor = {
        keymap = "vi";
        promptContext = true;
      };

      prompt.theme = "sorin";

      syntaxHighlighting.highlighters = [
        "main"
        "brackets"
        "pattern"
        "line"
        "cursor"
        "root"
      ];

      terminal.autoTitle = true;
      utility.safeOps = false;
    };
  };
}
