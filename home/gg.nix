{ lib, config, ... }:

{
  imports = [
    ./configuration.nix
    ./mixins/shell.nix
    ./mixins/prettier
    ./mixins/vcs.nix
  ];

  sops = {
    defaultSopsFile = ./data/personal.secrets.yaml;
    age.keyFile = "${config.home.homeDirectory}/.config/sops/age/keys.txt";
    gnupg.home = lib.mkForce null;
    secrets.gitconfig.path = "${config.home.homeDirectory}/.gitconfig.local";
  };

  programs.firefox.enable = true;

  programs.git.includes = [{ path = config.sops.secrets.gitconfig.path; }];

  # dconf = {
  #   enable = true;
  #
  #   settings = {
  #     "org/gnome/desktop/interface" = {
  #       clock-show-weekday = true;
  #     };
  #
  #     "org/gnome/desktop/input-sources" = {
  #       xkb-options = [ "caps:ctrl_modifier" ];
  #     };
  #   };
  # };

  fryngies = {
    ideavim.enable = true;
    neovim = {
      enable = true;
      withExtraLsp = true;
    };
  };

  programs.mangohud = {
    enable = true;
    enableSessionWide = true;
    settings = {
      full = true;
      no_display = true;
      cpu_load_change = true;
    };
  };

  home.stateVersion = "24.05";
}

