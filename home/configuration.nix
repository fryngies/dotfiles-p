{ pkgs, config, ... }:

{
  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;

  programs.gpg.enable = true;

  sops.gnupg.home = config.programs.gpg.homedir;

  programs.direnv = {
    enable = true;
    nix-direnv.enable = true;
  };

  programs.zsh.enable = true;
  programs.nix-index-database.comma.enable = true;
  programs.nix-index.enable = true;
  programs.bottom.enable = true;

  # Nicely reload system units when changing configs
  systemd.user.startServices = "sd-switch";

  home.packages = with pkgs; [
    devenv
    fd # faster find
    rsync # copy on steroids
    ncdu # directory size analyzer
    killall # kill processes by name
    nixos-option # observe nix attrs
    file # get file info
    universal-ctags # tags
    fzf # fuzzy finder
    ripgrep
    numi-cli
    nerd-fonts.jetbrains-mono
  ];

  fonts.fontconfig.enable = true;
}
