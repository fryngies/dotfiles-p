{ pkgs, config, ... }:

{
  imports = [
    ./configuration.nix
    ./mixins/shell.nix
    ./mixins/prettier
    ./mixins/vcs.nix
    ./mixins/iterm
  ];

  home.username = "fryngies";
  home.homeDirectory = "/Users/fryngies";

  sops = {
    defaultSopsFile = ./data/personal.secrets.yaml;
    secrets.gitconfig.path = "${config.home.homeDirectory}/.gitconfig.local";
  };

  programs.git.includes = [{ path = config.sops.secrets.gitconfig.path; }];

  fryngies = {
    ideavim.enable = true;
    neovim = {
      enable = true;
      withExtraLsp = true;
    };
  };

  home.file.".gnupg/gpg-agent.conf".text = ''
    pinentry-program ${pkgs.pinentry_mac}/${pkgs.pinentry_mac.binaryPath}
  '';

  home.packages = with pkgs; [
    colima
    jq
    ffmpeg
    openjdk17
    nodejs
  ];

  home.stateVersion = "23.11";
}
