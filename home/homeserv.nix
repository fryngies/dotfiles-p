{ pkgs, config, ... }:

{
  imports = [
    ./configuration.nix
    ./mixins/shell.nix
    ./mixins/gpg-agent.nix
    ./mixins/vcs.nix
  ];

  home.username = "fryngies";
  home.homeDirectory = "/home/fryngies";

  sops = {
    defaultSopsFile = ./data/personal.secrets.yaml;
    secrets.gitconfig.path = "${config.home.homeDirectory}/.gitconfig.local";
  };

  programs.git.includes = [{ path = config.sops.secrets.gitconfig.path; }];

  fryngies.neovim = {
    enable = true;
    withExtraLsp = false;
  };

  home.stateVersion = "22.11";

  home.packages = with pkgs; [
    reptyr # reparent a running program to a new terminal
  ];
}
