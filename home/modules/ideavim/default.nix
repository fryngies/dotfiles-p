{ config, lib, ... }:

{
  options.fryngies.ideavim = {
    enable = lib.mkEnableOption "ideavim";
  };

  config.home.file = lib.mkIf config.fryngies.ideavim.enable {
    ".ideavimrc".source = ./ideavimrc;
  };
}
