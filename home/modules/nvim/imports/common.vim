set nocompatible

set number
set relativenumber
set smartcase
set mouse=a
set autoindent
set smartindent
set shiftwidth=2
set tabstop=2
set hidden
set ignorecase
set smartcase
set scrolloff=2
set hlsearch
let mapleader=" "

nnoremap <silent> E :bp<CR>
nnoremap <silent> R :bn<CR>

nnoremap <silent> <ESC> :noh<CR>

map gF :e <cfile><CR>

nnoremap gf gF

noremap <CR> i<CR><ESC>^
