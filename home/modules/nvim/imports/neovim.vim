" vim-javascript
let g:javascript_plugin_jsdoc = 1

" Airline
let g:airline#extensions#tabline#enabled = 1
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#formatter = 'unique_tail_improved'

let g:markdown_fenced_languages = ['html', 'javascript', 'python', 'bash=sh']

" autocmd BufWritePre * :%s/\s\+$//
autocmd BufNewFile,BufRead .gitconfig.local setlocal ft=gitconfig

autocmd BufWrite * mkview
autocmd BufRead * silent! loadview

" keep folds and other stuff when closing file
" augroup autoview
" 	autocmd!
" 	autocmd BufWinLeave ?* call <SID>make_view()
" 	autocmd BufWinEnter ?* call <SID>load_view()
" augroup END
fun! s:make_view() abort
	if @% != "" && &foldmethod != 'diff'
		mkview!
	endif
endfun
fun! s:load_view() abort
	if @% != ""
		silent! loadview
	endif
endfun

" cleanup of temp files
" https://github.com/neovim/neovim/issues/21856
" augroup undofile_cleanup
" 	autocmd!
" 	autocmd VimLeavePre * call <sid>tempfile_cleanup()
" augroup END
" fun! s:tempfile_cleanup()
" 	" remove files older than 30 days
" 	exec "!find " . &undodir . " -type f -mtime +30 -exec rm {} \\;"
" endfun
