vim.keymap.set("n", "<Leader><Leader>", ":noh<CR>", { silent = true, desc = "Clear highlight" })

vim.opt.list = true
vim.opt.title = true
vim.opt.termguicolors = true
vim.opt.spell = true
vim.opt.signcolumn = "yes"

vim.undofile = true
vim.opt.undodir = vim.fn.expand("~/.local/share/nvim/undo/")
vim.opt.dir = vim.fn.expand("~/.local/share/nvim/swap/")

-- Give more space for displaying messages.
vim.opt.cmdheight = 2

vim.opt.foldenable = false

vim.opt.listchars = {
	lead = ".",
	tab = ">-",
	trail = "·",
	extends = "◣",
	precedes = "◢",
	nbsp = "␣",
}
