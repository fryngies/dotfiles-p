{ lib, config, options, ... }:

let
  cfg = config.programs.neovim.fryngies;
  nonEmptyStr = lib.types.addCheck lib.types.str (s: s != "");

  pluginWithConfigType = lib.types.submodule ({ config, ... }: {
    options = {
      plugin = lib.mkOption {
        type = lib.types.package;
        description = "Leading vim plugin.";
      };

      name = lib.mkOption {
        type = nonEmptyStr;
        description = "Name of the plugin.";
        default = config.plugin.pname;
      };

      config = lib.mkOption {
        type = lib.types.either lib.types.lines lib.types.path;
        description =
          "Lua script to configure this plugin.";
        default = /* lua */ ''
          require("${config.name}").setup({})
        '';
      };

      extraPackages = options.programs.neovim.extraPackages;

      extraPlugins = lib.mkOption {
        type = lib.types.listOf lib.types.package;
        description = "Extra associated plugins. Exists only for grouping purposes, the setup must be done manually.";
        default = [ ];
      };
    };
  });

  buildEntryFromPackage = package: {
    plugin = package;
    name = package.pname;
    config = /* lua */ ''
      require("${package.pname}").setup({})
    '';
    extraPlugins = [ ];
    extraPackages = [ ];
  };
in
{
  options.programs.neovim.fryngies.plugins = lib.mkOption {
    type = lib.types.listOf (lib.types.either lib.types.package pluginWithConfigType);
    default = [ ];
  };

  config.programs.neovim =
    let
      normalized =
        builtins.map
          (maybeEntry: if (lib.isDerivation maybeEntry) then (buildEntryFromPackage maybeEntry) else maybeEntry)
          cfg.plugins;

      plugins = (lib.lists.concatMap
        (entry:
          [
            {
              inherit (entry) plugin;
              type = "lua";

              config = if entry.config == "" then null else /* lua */ ''
                -- config for plugin: ${entry.name}
                do
                  -- local before = os.clock()

                  local function setup()
                    ${if (builtins.isPath entry.config) then (builtins.readFile entry.config) else entry.config}
                  end

                  local success, output = pcall(setup) -- execute 'setup()' and catch any errors

                  if not success then
                    vim.notify("Error on setup for module: ${entry.name}", vim.log.levels.ERROR)
                    vim.notify(output, vim.log.levels.ERROR)
                  end

                  -- vim.notify(
                  --   string.format("${entry.name}\t%0.6fs", os.clock() - before),
                  --   vim.log.levels.DEBUG
                  -- )
                end
              '';
            }
          ] ++ entry.extraPlugins)
        normalized);

      extraPackages = lib.lists.concatMap (p: p.extraPackages) normalized;
    in
    lib.mkIf (normalized != [ ]) {
      inherit plugins extraPackages;
    };
}

