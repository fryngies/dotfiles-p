{ lib, pkgs, config, ... }:

let
  cfg = config.fryngies.neovim;
in
{
  imports = [
    ./plugin.nix

    ./modules/lsp
    ./modules/telescope
    ./modules/theme
    ./modules/treesitter
    ./modules/vcs
    ./modules/trouble
  ];

  options.fryngies.neovim = {
    enable = lib.mkEnableOption "fryngies vim";
    withExtraLsp = lib.mkEnableOption "extra lsp servers and related packages";
  };

  config = lib.mkIf cfg.enable {
    programs.neovim = {
      enable = true;

      extraLuaConfig = ''
        ${lib.concatStringsSep "\n" (map (vimf: ''
          vim.cmd [[
          ${lib.fileContents vimf}
          ]]
        '') [ ./imports/common.vim ./imports/neovim.vim ])}
        
        ${builtins.readFile ./imports/neovim.lua}
      '';

      defaultEditor = true;

      viAlias = true;
      vimAlias = true;
      vimdiffAlias = true;
      withNodeJs = true;
      withPython3 = true;

      fryngies = {
        lsp = {
          enable = true;
          inherit (cfg) withExtraLsp;
        };
        telescope.enable = true;
        theme.enable = true;
        treesitter.enable = true;
        vcs.enable = true;
        trouble.enable = true;

        plugins = with pkgs.vimPlugins; [
          # set commentstring based on treesitter context
          # it seem to be working in some files already?
          {
            plugin = nvim-ts-context-commentstring;
            name = "ts_context_commentstring";
            config = /* lua */ ''
              vim.g.skip_ts_context_commentstring_module = true
              require('ts_context_commentstring').setup({
                enable_autocmd = false,
              })
              -- enable for built-in commenting https://github.com/JoosepAlviste/nvim-ts-context-commentstring/wiki/Integrations#native-commenting-in-neovim-010
              local get_option = vim.filetype.get_option
              vim.filetype.get_option = function(filetype, option)
                return option == "commentstring"
                  and require("ts_context_commentstring.internal").calculate_commentstring()
                  or get_option(filetype, option)
              end
            '';
          }
          { plugin = nvim-web-devicons; config = ""; }
          {
            plugin = dressing-nvim;
            config = /* lua */ ''
              require('dressing').setup({
                start_in_insert = false,
              })
            '';
          }
          nvim-surround
          # continue bullet lists
          { plugin = bullets-vim; config = ""; }
          # highlight words under the cursor
          { plugin = vim-illuminate; config = ""; }
          {
            plugin = lualine-nvim;
            config = /* lua */ ''
              require('lualine').setup({ theme = 'catpuccin' })
            '';
          }

          # highlight findable chars in current line
          { plugin = quick-scope; config = ""; }
          # hash mocking
          { plugin = vim-nixhash; config = ""; }

          # adjust shiftwidth and expandtab based on current file, nearby files or editorconfig
          { plugin = vim-sleuth; config = ""; }

          {
            plugin = nvim-autopairs;
            config = /* lua */ ''
              require('nvim-autopairs').setup()

              local cmp_autopairs = require('nvim-autopairs.completion.cmp')
              local cmp = require('cmp')
              cmp.event:on(
                'confirm_done',
                cmp_autopairs.on_confirm_done()
              )
            '';
          }
        ];
      };
    };

    home.sessionVariables.VISUAL = "nvim";
  };
}
