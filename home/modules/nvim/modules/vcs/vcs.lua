local neogit = require("neogit")

neogit.setup({})

require("gitsigns").setup({
	on_attach = function(bufnr)
		local gitsigns = require("gitsigns")

		local function map(mode, l, r, opts)
			opts = opts or {}
			opts.buffer = bufnr
			vim.keymap.set(mode, l, r, opts)
		end

		-- Navigation
		map("n", "]c", function()
			if vim.wo.diff then
				vim.cmd.normal({ "]c", bang = true })
			else
				gitsigns.nav_hunk("next")
			end
		end, { desc = "Go to next changed chunk" })

		map("n", "[c", function()
			if vim.wo.diff then
				vim.cmd.normal({ "[c", bang = true })
			else
				gitsigns.nav_hunk("prev")
			end
		end, { desc = "Go to previous changed chunk." })

		map("n", "<Leader>vu", gitsigns.reset_hunk, { desc = "Reset chunk under the cursor" })
		map("v", "<Leader>vu", function()
			gitsigns.reset_hunk({ vim.fn.line("."), vim.fn.line("v") })
		end, { desc = "Reset selected chunk" })
		map("n", "<Leader>vU", gitsigns.reset_buffer, { desc = "Reset all chunks in current buffer" })

		map("n", "<Leader>vK", gitsigns.preview_hunk, { desc = "Preview change" })

		map("n", "<Leader>vb", function()
			gitsigns.blame_line({ full = true })
		end, { desc = "Blame" })

		map("n", "<Leader>vd", gitsigns.diffthis, { desc = "Open diff" })
		map("n", "<Leader>vtd", gitsigns.toggle_deleted, { desc = "Preview deleted lines" })

		map("v", "<leader>vs", function()
			gitsigns.stage_hunk({ vim.fn.line("."), vim.fn.line("v") })
		end, { desc = "Stage chunk under the cursor" })
		map("v", "<leader>vS", gitsigns.stage_buffer, { desc = "Stage current buffer" })
	end,
})

vim.keymap.set("n", "<Leader>vg", function()
	neogit.open({
		kind = "auto", -- auto (v)split
	})
end, { desc = "Open Neogit pane" })
