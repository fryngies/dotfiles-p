{ config, pkgs, lib, ... }:

let
  cfg = config.programs.neovim.fryngies.vcs;
in
{
  options.programs.neovim.fryngies.vcs = {
    enable = lib.mkEnableOption "neovim vcs";
  };

  config.programs.neovim.fryngies.plugins = with pkgs.vimPlugins; lib.mkIf cfg.enable [
    # Git wrapper
    {
      plugin = neogit;
      config = ./vcs.lua;
      extraPlugins = [
        gitsigns-nvim
        diffview-nvim
      ];
    }
  ];
}
