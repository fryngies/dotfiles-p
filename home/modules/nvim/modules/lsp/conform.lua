local conform = require("conform")
local gitsigns = require("gitsigns")

-- buggy fts
local ignored_hunk_format_fts = {
	"lua",
	"nix",
}

conform.setup({
	formatters_by_ft = {
		lua = { "stylua" },
		rust = { "rustfmt", lsp_format = "fallback" },

		javascript = { "prettierd" },
		typescript = { "prettierd" },
		markdown = { "prettierd" },
		html = { "prettierd" },
		json = { "prettierd" },

		nix = { "nixpkgs_fmt" },
	},
})

local function format(args)
	conform.format(args)
end

local function format_changed(args, hunks)
	for i = #hunks, 1, -1 do
		local hunk = hunks[i]

		if hunk ~= nil and hunk.type ~= "delete" then
			local start = hunk.added.start
			local last = start + hunk.added.count

			-- nvim_buf_get_lines uses zero-based indexing -> subtract from last
			local last_hunk_line = vim.api.nvim_buf_get_lines(0, last - 2, last - 1, true)[1]
			local range = { start = { start, 0 }, ["end"] = { last - 1, last_hunk_line:len() } }

			format({ range = range, unpack(args) })
		end
	end
end

local function format_cmd(args)
	local hunks = gitsigns.get_hunks()

	if hunks == nil then
		vim.notify("got no hunks, falling back to full formatting.", "info", { title = "formatting" })
		return format(args)
	end

	if vim.tbl_contains(ignored_hunk_format_fts, vim.bo.filetype) then
		vim.notify("range formatting for " .. vim.bo.filetype .. " not working properly.")
		return format(args)
	end

	return format_changed(args, hunks)
end

local function setup_autoformat(gname)
	local gid = vim.api.nvim_create_augroup(gname, {})

	vim.api.nvim_create_autocmd("BufWritePre", {
		pattern = "*",
		callback = function()
			format_cmd({})
		end,
		group = gid,
	})
end

local function setup_format()
	vim.keymap.set("n", "<Leader>cf", format_cmd, { desc = "Format current buffer" })
	vim.api.nvim_create_user_command("Format", function()
		format_cmd({})
		print("Formatted")
	end, { desc = "Format current buffer" })

	vim.keymap.set("n", "<Leader>cF", format, { desc = "Format all lines in current buffer" })
	vim.api.nvim_create_user_command("FormatAll", function()
		format({})
		print("Formatted all")
	end, { desc = "Format all lines in current buffer" })

	local autoformatting = "autoformatting"
	local autoformatting_enabled = true

	setup_autoformat(autoformatting)

	vim.keymap.set("n", "<Leader>tcf", function()
		if autoformatting_enabled then
			vim.api.nvim_del_augroup_by_name(autoformatting)
			autoformatting_enabled = false
			print("Autoformatting off")
		else
			setup_autoformat(autoformatting)
			autoformatting_enabled = true
			print("Autoformatting on")
		end
	end, { desc = "Toggle autoformat" })
end

setup_format()
