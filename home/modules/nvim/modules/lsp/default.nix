{ config, pkgs, lib, ... }:

let cfg = config.programs.neovim.fryngies.lsp;
in {
  options.programs.neovim.fryngies.lsp = {
    enable = lib.mkEnableOption "neovim lsp";
    withExtraLsp = lib.mkEnableOption "extra language servers";
  };

  config.programs.neovim.fryngies.plugins = with pkgs.vimPlugins; lib.mkIf cfg.enable [
    # completion ui
    {
      plugin = nvim-cmp;
      name = "cmp";
      extraPlugins = [
        # lsp completions
        cmp-nvim-lsp
        # buffer completions
        cmp-buffer
        # command line completions
        cmp-cmdline
        cmp-under-comparator
        # snippet engine
        luasnip
      ];
    }
    # embedded languages
    { plugin = otter-nvim; name = "otter"; }
    # tsserver support
    { plugin = typescript-tools-nvim; name = "typescript-tools"; }
    # LSPs integration
    {
      plugin = nvim-lspconfig;
      config = ./lspconfig.lua;
      extraPackages = with pkgs;
        [
          watchman
          fd

          lua-language-server
          yaml-language-server
          nil
          pyright
          nixpkgs-fmt

          nodePackages.bash-language-server
          shellcheck
          stylua
        ] ++ (with pkgs.nodePackages; lib.optionals cfg.withExtraLsp [
          typescript
          typescript-language-server
          # HTML/CSS/JSON/ESLint
          vscode-langservers-extracted
          prettier
          pkgs.prettierd
          eslint
          eslint_d
        ]) ++ (with pkgs; lib.optionals cfg.withExtraLsp [
          # broken on darwin
          # nodePackages.svelte-check
          nodePackages.svelte-language-server

          rust-analyzer
          rustfmt
          rustPackages.clippy
          terraform-ls
        ]);
    }
    # formatting
    { plugin = conform-nvim; config = ./conform.lua; }
    # debugging
    { plugin = nvim-dap; config = ""; extraPlugins = [ plenary-nvim ]; }
    # breadcrumbs
    { plugin = dropbar-nvim; config = ./dropbar.lua; }

    # https://github.com/b0o/SchemaStore.nvim
    # A Neovim plugin that provides the SchemaStore catalog for use with jsonls and yamlls.
    { plugin = SchemaStore-nvim; config = ./schemastore.lua; }
  ];
}
