local api = require("dropbar.api")
local utils = require("dropbar.utils")

require("dropbar").setup({
	menu = {
		preview = false,
		keymaps = {
			["<C-p>"] = "<Up>",
			["<C-n>"] = "<Down>",
		},
	},
	fzf = {
		keymaps = {
			["<C-p>"] = api.fuzzy_find_prev,
			["<C-n>"] = api.fuzzy_find_next,
		},
	},
})

vim.keymap.set("n", "<M-Up>", function()
	api.pick()
end, { desc = "Pick breadcrumb" })
