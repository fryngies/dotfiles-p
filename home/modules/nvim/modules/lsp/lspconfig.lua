local lspconfig = require("lspconfig")
local lsp_defaults = lspconfig.util.default_config

lsp_defaults.capabilities =
	vim.tbl_deep_extend("force", lsp_defaults.capabilities, require("cmp_nvim_lsp").default_capabilities())

lspconfig.lua_ls.setup({
	on_init = function(client)
		local path = client.workspace_folders[1].name
		if vim.loop.fs_stat(path .. "/.luarc.json") or vim.loop.fs_stat(path .. "/.luarc.jsonc") then
			return
		end

		client.config.settings.Lua = vim.tbl_deep_extend("force", client.config.settings.Lua, {
			runtime = {
				-- Tell the language server which version of Lua you're using
				-- (most likely LuaJIT in the case of Neovim)
				version = "LuaJIT",
			},
			-- Make the server aware of Neovim runtime files
			workspace = {
				checkThirdParty = false,
				library = {
					vim.env.VIMRUNTIME,
					-- Depending on the usage, you might want to add additional paths here.
					-- "${3rd}/luv/library"
					-- "${3rd}/busted/library",
				},
				ignoreDir = { ".vscode", ".idea", ".direnv" },
				-- or pull in all of 'runtimepath'. NOTE: this is a lot slower
				-- library = vim.api.nvim_get_runtime_file("", true)
			},
		})
	end,
})

lspconfig.svelte.setup({})

-- Nix
lspconfig.nil_ls.setup({})
-- Bash
lspconfig.bashls.setup({})
-- Python
lspconfig.pyright.setup({})
-- JSON
lspconfig.jsonls.setup({})
-- Rust
lspconfig.rust_analyzer.setup({
	settings = {
		["rust-analyzer"] = {
			files = {
				excludeDirs = { ".direnv" },
			},
		},
	},
})
-- Haskell
lspconfig.hls.setup({})
-- ESLint
lspconfig.eslint.setup({})
lspconfig.terraformls.setup({})

vim.keymap.set("n", "<leader>d", vim.diagnostic.open_float, { desc = "Show diagnostics under cursor" })
vim.keymap.set("n", "<leader>q", vim.diagnostic.setloclist, { desc = "Add buffer diagnostics to the loclist" })

-- Use LspAttach autocommand to only map the following keys
-- after the language server attaches to the current buffer
vim.api.nvim_create_autocmd("LspAttach", {
	group = vim.api.nvim_create_augroup("UserLspConfig", {}),
	callback = function(ev)
		-- Buffer local mappings.
		-- See `:help vim.lsp.*` for documentation on any of the below functions
		local opts = { buffer = ev.buf }
		local telescope_builtin = require("telescope.builtin")
		vim.keymap.set("n", "gD", vim.lsp.buf.declaration, { unpack(opts), desc = "Go to declaration" })
		vim.keymap.set("n", "gd", telescope_builtin.lsp_definitions, { unpack(opts), desc = "Go to definition" })
		vim.keymap.set("n", "gs", telescope_builtin.lsp_document_symbols, { unpack(opts), desc = "List symbols" })
		vim.keymap.set(
			"n",
			"gi",
			telescope_builtin.lsp_implementations,
			{ unpack(opts), desc = "Go to implementation" }
		)
		vim.keymap.set(
			"n",
			"<leader>wa",
			vim.lsp.buf.add_workspace_folder,
			{ unpack(opts), desc = "Add workspace folder" }
		)
		vim.keymap.set(
			"n",
			"<leader>wr",
			vim.lsp.buf.remove_workspace_folder,
			{ unpack(opts), desc = "Remove workspace folder" }
		)
		vim.keymap.set("n", "<leader>wl", function()
			print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
		end, { unpack(opts), desc = "List workspaces" })
		vim.keymap.set("n", "<leader>D", vim.lsp.buf.type_definition, { unpack(opts), desc = "Go to type definition" })
		vim.keymap.set("n", "<leader>r", vim.lsp.buf.rename, { unpack(opts), desc = "Rename" })
		vim.keymap.set({ "n", "v" }, "<M-CR>", vim.lsp.buf.code_action, { unpack(opts), desc = "Code actions" })
		vim.keymap.set("n", "gr", telescope_builtin.lsp_references, { unpack(opts), desc = "Show references" })
		vim.keymap.set(
			{ "n", "v", "i" },
			"<C-k>",
			vim.lsp.buf.signature_help,
			{ unpack(opts), desc = "Show signature" }
		)
	end,
})

local cmp = require("cmp")

cmp.setup({
	snippet = {
		-- REQUIRED - you must specify a snippet engine
		expand = function(args)
			require("luasnip").lsp_expand(args.body) -- For `luasnip` users.
		end,
	},
	window = {
		completion = cmp.config.window.bordered(),
		documentation = cmp.config.window.bordered(),
	},
	mapping = cmp.mapping.preset.insert({
		["<C-b>"] = cmp.mapping.scroll_docs(-4),
		["<C-f>"] = cmp.mapping.scroll_docs(4),
		["<C-s>"] = cmp.mapping.complete(),
		["<C-e>"] = cmp.mapping.abort(),
		["<CR>"] = cmp.mapping.confirm({ select = true }), -- Accept currently selected item. Set `select` to `false` to only confirm explicitly selected items.
	}),
	sources = cmp.config.sources({
		{ name = "nvim_lsp" },
		{ name = "luasnip" },
		-- { name = "otter" },
	}, {
		{ name = "fuzzy_path" },
		{ name = "buffer" },
	}),
	sorting = {
		comparators = {
			cmp.config.compare.offset,
			cmp.config.compare.exact,
			cmp.config.compare.score,
			cmp.config.compare.recently_used,
			require("cmp-under-comparator").under,
			cmp.config.compare.kind,
		},
	},
})

cmp.setup.filetype("gitcommit", {
	sources = cmp.config.sources({
		{ name = "git" }, -- You can specify the `git` source if [you were installed it](https://github.com/petertriho/cmp-git).
	}, {
		{ name = "buffer" },
	}),
})

cmp.setup.cmdline({ "/", "?" }, {
	mapping = cmp.mapping.preset.cmdline(),
	sources = {
		{ name = "buffer" },
	},
})

cmp.setup.cmdline(":", {
	mapping = cmp.mapping.preset.cmdline(),
	sources = cmp.config.sources({
		{ name = "cmdline" },
	}, {
		-- { name = "fuzzy_path", option = { fd_timeout_msec = 1500 } },
	}),
})
