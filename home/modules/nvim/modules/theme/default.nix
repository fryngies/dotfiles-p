{ config, pkgs, lib, ... }:

let
  cfg = config.programs.neovim.fryngies.theme;
in
{
  options.programs.neovim.fryngies.theme = {
    enable = lib.mkEnableOption "neovim theme";
  };

  config.programs.neovim.fryngies.plugins = with pkgs.vimPlugins; lib.mkIf cfg.enable [
    {
      plugin = catppuccin-nvim;
      config = /* lua */ ''
        require("catppuccin").setup({
          dropbar = {
            enable = true,
            color_mode = true,
          },
          lsp_trouble = true,
        })
        vim.cmd.colorscheme("catppuccin") -- catppuccin-latte, catppuccin-frappe, catppuccin-macchiato, catppuccin-mocha
      '';
    }
  ];
}
