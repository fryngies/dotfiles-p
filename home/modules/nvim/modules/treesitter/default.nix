{ config, pkgs, lib, ... }:

let
  cfg = config.programs.neovim.fryngies.treesitter;
in
{
  options.programs.neovim.fryngies.treesitter = {
    enable = lib.mkEnableOption "neovim treesitter";
  };

  config.programs.neovim.fryngies.plugins = with pkgs.vimPlugins; lib.mkIf cfg.enable [
    {
      plugin = pkgs.vimPlugins.nvim-treesitter.withAllGrammars;
      config = ./nvim-treesitter.lua;
      extraPlugins = [
        # close html tags automatically
        nvim-ts-autotag
      ];
    }
  ];
}
