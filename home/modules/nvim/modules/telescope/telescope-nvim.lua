local telescope = require("telescope")
local builtin = require("telescope.builtin")

telescope.setup({
	extensions = {
		undo = {
			mappings = {
				i = {
					["<M-CR>"] = require("telescope-undo.actions").restore,
				},
			},
		},
	},
})

require("neoclip").setup()

telescope.load_extension("file_browser")
telescope.load_extension("project")
telescope.load_extension("undo")
telescope.load_extension("neoclip")
telescope.load_extension("git_diffs")

local function find(extraArgs)
	extraArgs = extraArgs or {}
	return function()
		builtin.find_files({
			find_command = {
				"fd",
				"--type",
				"f",
				"--color=never",
				"--hidden",
				"--follow",
				"--ignore-file=.arcignore",
				"-E",
				".git/*",
				unpack(extraArgs),
			},
		})
	end
end

-- fuzzy find
vim.keymap.set("n", "<leader>ff", find(), { desc = "Find file in the current file directory" })

-- fuzzy find on ignored files
vim.keymap.set(
	"n",
	"<leader>fF",
	find({ "--ignore" }),
	{ desc = "Find file the current file directory ignoring ignore file" }
)

vim.keymap.set("n", "<leader>fg", function()
	telescope.extensions.live_grep_args.live_grep_args({
		additional_args = { "--ignore-file=.arcignore" },
	})
end, { desc = "Grep text" })
local live_grep_args_shortcuts = require("telescope-live-grep-args.shortcuts")
vim.keymap.set("n", "<leader>fG", function()
	live_grep_args_shortcuts.grep_word_under_cursor({})
end, { desc = "Grep word under cursor" })
vim.keymap.set("v", "<leader>fg", live_grep_args_shortcuts.grep_visual_selection, { desc = "Grep selection" })
vim.keymap.set("n", "<leader>fb", builtin.buffers, { desc = "Open list of buffers" })
vim.keymap.set("n", "<leader>fh", builtin.help_tags, { desc = "Find help" })

vim.keymap.set(
	"n",
	"<Leader>fe",
	":Telescope file_browser path=%:p:h select_buffer=true<CR>",
	{ silent = true, desc = "Open file browser" }
)
vim.keymap.set(
	"n",
	"<Leader>fE",
	":Telescope file_browser<CR>",
	{ silent = true, desc = "Open file browser in project root" }
)

vim.keymap.set("n", "<Leader>fr", ":Telescope frecency workspace=CWD", { silent = true, desc = "Open frecency" })

vim.keymap.set("n", "<leader>fu", ":Telescope undo<CR>", { silent = true, desc = "Open undo list" })

vim.keymap.set("n", "<Leader>fy", ":Telescope neoclip<CR>", { silent = true, desc = "Open copy history" })

vim.keymap.set("n", "<Leader>fk", ":Telescope keymaps<CR>", { silent = true, desc = "Open list of keymaps" })
