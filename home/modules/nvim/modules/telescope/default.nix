{ config, lib, pkgs, ... }:

let cfg = config.programs.neovim.fryngies.telescope;
in {
  options.programs.neovim.fryngies.telescope = {
    enable = lib.mkEnableOption "neovim telescope";
  };

  config.programs.neovim.fryngies.plugins = with pkgs.vimPlugins; lib.mkIf cfg.enable [
    {
      plugin = telescope-nvim;
      config = ./telescope-nvim.lua;
      extraPlugins = [
        telescope-fzf-native-nvim
        telescope-file-browser-nvim
        telescope-project-nvim
        telescope-frecency-nvim
        telescope-live-grep-args-nvim
        telescope-undo-nvim
        telescope-git-diffs-nvim
        sqlite-lua
        nvim-neoclip-lua
      ];
      extraPackages = [
        # required by telescope live_grep
        pkgs.ripgrep
        pkgs.fd
        pkgs.git
      ];
    }
  ];
}
