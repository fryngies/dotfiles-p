{ config, lib, pkgs, ... }:

let cfg = config.programs.neovim.fryngies.trouble;
in {
  options.programs.neovim.fryngies.trouble = {
    enable = lib.mkEnableOption "neovim trouble plugin";
  };

  config.programs.neovim = lib.mkIf cfg.enable {
    plugins = with pkgs.vimPlugins; [
      {
        type = "lua";
        plugin = trouble-nvim;
        config = builtins.readFile ./trouble-nvim.lua;
      }
    ];
  };
}
