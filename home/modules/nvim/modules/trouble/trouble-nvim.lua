require("trouble").setup({})

vim.keymap.set("n", "<Leader>xx", ":Trouble diagnostics toggle<CR>", { desc = "Toggle diagnostics panel" })
vim.keymap.set(
	"n",
	"<Leader>xX",
	":Trouble diagnostics toggle filter.buf=0<cr>",
	{ desc = "Toggle diagnostics panel for current buffer" }
)
vim.keymap.set("n", "<Leader>cs", ":Trouble symbols toggle focus=false<cr>", { desc = "Toggle symbols panel" })
vim.keymap.set(
	"n",
	"<Leader>cl",
	":Trouble lsp toggle focus=false win.position=right<cr>",
	{ desc = "Toggle LSP panel" }
)
vim.keymap.set("n", "<Leader>xL", ":Trouble loclist toggle<cr>", { desc = "Toggle loclist panel" })
vim.keymap.set("n", "<Leader>xQ", ":Trouble qflist toggle<cr>", { desc = "Toggle quickfix panel" })
