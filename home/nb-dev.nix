{
  imports = [
    ./configuration.nix
    ./mixins/shell.nix
  ];
  home.username = "fryngies";
  home.homeDirectory = "/home/fryngies";

  fryngies = {
    neovim = {
      enable = true;
      withExtraLsp = true;
    };
  };

  home.stateVersion = "23.11";
}
