final: prev:

{
  overseerr = import ../packages/overseerr.nix final;
  numi-cli = import ../packages/numi-cli.nix final;

  vimPlugins = (import ../packages/vim-plugins { pkgs = final; }) // prev.vimPlugins;
}
