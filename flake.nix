{
  description =
    "Fryngies dotfiles. Contains Nix configuration (including dotfiles) for my devices.";

  inputs = {
    nixpkgs.url = "nixpkgs/nixos-unstable";

    darwin = {
      url = "github:lnl7/nix-darwin/master";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    sops-nix = {
      url = "github:Mic92/sops-nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    chikin-icons = {
      url = "git+ssh://git@gitlab.com/fryngies/chikin-icons";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    nix-index-database = {
      url = "github:Mic92/nix-index-database";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    devenv.url = "github:cachix/devenv";

    tasmota-exporter = {
      url = "github:kradalby/tasmota-exporter";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    disko = {
      url = "github:nix-community/disko";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    lanzaboote = {
      url = "github:nix-community/lanzaboote/v0.4.2";

      # Optional but recommended to limit the size of your system closure.
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs =
    { nixpkgs
    , darwin
    , home-manager
    , sops-nix
    , chikin-icons
    , nix-index-database
    , devenv
    , tasmota-exporter
    , disko
    , lanzaboote
    , self
    }:
    let
      forEachSystem = f: nixpkgs.lib.genAttrs [ "x86_64-linux" "aarch64-darwin" ] (system: f nixpkgs.legacyPackages.${system});
      nixpkgsConfig = import ./nixpkgs-config.nix { inherit self nixpkgs; };

      mkDarwinConfiguration = { configurationPath, users, extraModules ? [ ] }:
        darwin.lib.darwinSystem {
          system = "aarch64-darwin";
          modules = [
            nixpkgsConfig

            {
              nixpkgs.overlays = [
                self.overlays.default
                devenv.overlays.default
              ];
            }

            {
              nix.gc = {
                automatic = true;
                options = "--delete-older-than 14d";
              };
            }

            home-manager.darwinModules.home-manager

            configurationPath

            { home-manager.sharedModules = [ self.homeManagerModules.default ]; }
            ./home/module.nix
            { home-manager = { inherit users; }; }
          ] ++ extraModules;
        };
    in
    {
      formatter = forEachSystem (pkgs: pkgs.nixpkgs-fmt);

      overlays.default = import ./overlays;

      packages = forEachSystem (pkgs: {
        overseerr = import ./packages/overseerr.nix pkgs;
        numi-cli = import ./packages/numi-cli.nix pkgs;
      });

      nixosModules = {
        overseerr = ./nixos/modules/services/overseerr.nix;
        zomboid = ./nixos/modules/services/zomboid.nix;
        virtualisation = ./nixos/modules/virtualisation;
      };

      homeManagerModules.default = {
        imports = [
          ./home/modules
          nix-index-database.hmModules.nix-index
          sops-nix.homeManagerModules.sops
        ];
      };

      devShells = forEachSystem (pkgs: import ./shell.nix { inherit pkgs; });

      nixosConfigurations =
        let
          mkConfiguration = { configurationPath, users, extraModules ? [ ] }:
            nixpkgs.lib.nixosSystem {
              system = "x86_64-linux";
              modules = [
                nixpkgsConfig

                {
                  nixpkgs.overlays = [
                    self.overlays.default
                    devenv.overlays.default
                    tasmota-exporter.overlay
                  ];
                }

                {
                  nix = {
                    # periodically garbage collect
                    gc = {
                      automatic = true;
                      dates = "04:30";
                      options = "--delete-older-than 14d";
                    };
                  };
                }

                home-manager.nixosModules.home-manager
                sops-nix.nixosModules.sops

                configurationPath

                { home-manager.sharedModules = [ self.homeManagerModules.default ]; }
                ./home/module.nix
                { home-manager = { inherit users; }; }
              ] ++ extraModules;
            };
        in
        {
          homeserv = mkConfiguration {
            configurationPath = ./hosts/homeserv/os/configuration.nix;
            users.fryngies = ./home/homeserv.nix;
            extraModules = [
              chikin-icons.nixosModules.default
              tasmota-exporter.nixosModules.default

              self.nixosModules.overseerr
              self.nixosModules.zomboid
              self.nixosModules.virtualisation
            ];
          };

          gg = mkConfiguration {
            configurationPath = ./hosts/gg/configuration.nix;
            users.fryngies = ./home/gg.nix;
            extraModules = [
              lanzaboote.nixosModules.lanzaboote
              disko.nixosModules.disko
            ];
          };
        };

      lib = {
        inherit mkDarwinConfiguration;
      };

      darwinConfigurations = {
        dg-mba = mkDarwinConfiguration {
          configurationPath = ./hosts/dg-mba/configuration.nix;
          users.fryngies = ./home/dg-mba.nix;
        };
      };
    };
}
